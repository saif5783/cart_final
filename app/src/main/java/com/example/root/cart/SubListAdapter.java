package com.example.root.cart;

/**
 * Created by root on 14/7/15.
 */
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

public class SubListAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Subgroup> objects;

    SubListAdapter(Context context, ArrayList<Subgroup> subgroup) {
        ctx = context;
        objects = subgroup;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.subgrouplist, parent, false);
        }

        Subgroup p = getProduct(position);

        ((TextView) view.findViewById(R.id.subgrpname)).setText(p.name);


        CheckBox cbBuy = (CheckBox) view.findViewById(R.id.subcheckbox);
        cbBuy.setOnCheckedChangeListener(myCheckChangList);
        cbBuy.setTag(position);
        cbBuy.setChecked(p.box);
        return view;
    }

    Subgroup getProduct(int position) {
        return ((Subgroup) getItem(position));
    }

    ArrayList<Subgroup> getBox() {
        ArrayList<Subgroup> box = new ArrayList<Subgroup>();
        for (Subgroup p : objects) {
            //if (p.box)
            {
                System.out.println("subname------>"+p.name +" " +p.position+"  "+p.box);
                box.add(p);

            }

        }
        return box;
    }

    OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getProduct((Integer) buttonView.getTag()).box = isChecked;
           // if(isChecked==true)
            getProduct((Integer) buttonView.getTag()).position=(Integer) buttonView.getTag();
        }
    };
}

