package com.example.root.cart;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.InputStream;

/**
 * Created by Saifur on 6/9/2015.
 */
public class HelpDoc extends Activity
{WebView wv;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wv = new WebView(this);
        setContentView(wv);
       /* try
        {
            InputStream stream= this.getAssets().open("HelpDoc.html");
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            String html = new String(buffer);
            wv.loadData(html,"text/html","UTF-8");
        }catch(Exception e)
        {
         e.printStackTrace();
        }*/
        StringBuilder html = new StringBuilder();

        html.append("<html>\n" +
                "<head>\n" +
                "<h2></strong>Using the Cart app</h2>\n" +
                "</head>\n" +
                "<body>\n" +
                "<a name=\"Top\"></a>\n" +
                "<p>This app has been developed to help you get the ingredients for your food at your doorstep. You can order from the local farmer groups of your choice.<br>This document contains information on how to use the app. You can refer to it whenever you need help.\n" +
                "</p>\t<ol start=\"1\"> <li><a href=\"#Login\" style=\"text-decoration:none\">Logging in </a></li>\n" +
                "                       <li><a href=\"#Forgot\" style=\"text-decoration:none\">Forgot Password</a></li>\n" +
                "                       <li><a href=\"#Dashboard\" style=\"text-decoration:none\">The Dashboard </a></li>\n" +
                "                       <li><a href=\"#NewOrder\" style=\"text-decoration:none\">Placing an order</a></li>\n" +
                "                       <li><a href=\"#PrevOrder\" style=\"text-decoration:none\">Looking at your previous orders</a></li>\n" +
                "                       <li><a href=\"#CancelOrder\" style=\"text-decoration:none\">Cancelling an order</a></li> \n" +
                "                       <li><a href=\"#Draft\" style=\"text-decoration:none\">Saving your orders in Drafts</a></li>\n" +
                "        </ol>\n" +
                "\n" +
                "\n" +
                "<p>\n" +
                "<a name=\"Login\"><b>LOGGING IN</b></a><br>This is the Sign In interface of the app.<br> Press the \"Sign In\" button.\n" +
                "<br><img src=\"main.png\">\n" +
                "    <br>\n" +
                "<br>Now for Login\n" +
                "    <br><img src=\"login.png\">\n" +
                "    <ul>\n" +
                "    <li>Enter mobile number registered with the organization.</li>\n" +
                "    <li>Enter password</li>\n" +
                "    <li>Press “<b>Login</b>” button.</li>\n" +
                "    </ul>\n" +
                "<br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                "</p>\n" +
                "\n" +
                "\n" +
                "<p>\n" +
                "<a name=\"Forgot\"><b>FORGOT PASSWORD</b></a><br>In case you forgot password, OR you don't have one, then press \"<b>Forgot Password</b>\" button.<br>It will open a dialog box, like this:\n" +
                "    <br><img src=\"otp.png\">\n" +
                "    <br>\n" +
                "    <br>Now for setting new password:\n" +
                "    <br>\n" +
                "<ul>\n" +
                "    <li>Enter mobile number registered with the organization.</li>\n" +
                "    <li>Click on the \"<b>Forgot Password</b>\" button.</li>\n" +
                "    <li>Click on \"<b>Get OTP</b>\" button in the dialog box that appears.</li>\n" +
                "    <li>Enter the OTP received; you will get this by text message on your registered mobile number.</li>\n" +
                "    <li>Enter new password for Login</li>\n" +
                "    <li>Confirm your password.</li>\n" +
                "    <li>Click on \"<b>Confirm</b>\" button.</li>\n" +
                "</ul>\n" +
                "<br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                "\n" +
                "</p>\n" +
                "\n" +
                "\n" +
                "<p>\n" +
                "<a name=\"Dashboard\"><b>THE DASHBOARD</b></a><br>This is the home interface of the app.\n" +
                "<br>The name of the Organisation you are registered with is shown in the top.\n" +
                "<br><img src=\"dashboard2.png\">\n" +
                "<br>The title bar contains the \"Help\" menu button where you can get information regarding us.\n" +
                "<br>It also has the help document which you can refer to when in doubt.\n" +
                "<br>A \"<b>Logout</b>\" button is also provided, in case you want to log out from the app.\n" +
                "<br><img src=\"help.png\">\n" +
                "<br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                "</p>\n" +
                "\n" +
                "<p>\n" +
                "<a name=\"NewOrder\"><b>PLACING A NEW ORDER</b></a>\n" +
                "<br><ul><li>STEP 1: Choose the organisation and group from which you want to order. (as mentioned <a href=\"#Dashboard\" style=\"text-decoration:none\">here</a>)</li>\n" +
                "        <li>STEP 2: Click on the \"<b>New Order</b>\" button in the Dashboard.<br>This takes you to the New order interface.\n" +
                "                   <br><img src=\"newOrder.png\"></li>\n" +
                "        <li>STEP 3: Enter the quantity of each item that you want to order.</li>\n" +
                "        <li>STEP 4: Click on the \"<b>Proceed</b>\" button.</li>\n" +
                "        <li>STEP 5: Here you are shown your full order and the amount to be paid.<br>Click on \"<b>Check Out</b>\" to order your list of items.\n" +
                "                   <br><img src=\"confirm.png\"></li>\n" +
                "        <li>STEP 6: You will get a confirmation page. Click on \"<b>Done</b>\"\n" +
                "                   <br><img src=\"submit.png\" alt=\"Google logo\"></li>\n" +
                "        </ul>\n" +
                "<br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                "</p>\n" +
                "\n" +
                "<p>\n" +
                "<a name=\"PrevOrder\"><b>LOOKING AT THE PREVIOUS ORDERS</b></a>\n" +
                "<br>Clicking on the \"<b>Previous Orders</b>\" button helps you look at the orders that have been processed by the organisation, and have reached <b>or</b> are about to reach you.\n" +
                "<br><img src=\"prev.png\">\n" +
                "<br>On clicking each of the orders, you get a detailed view of the order. \n" +
                "<br><img src=\"vieworder.png\">\n" +
                "<br>Here you can click on \"<b>Modify</b>\" button to modify that order and place it, the same way we did before.\n" +
                "<br>\n" +
                "<br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                "</p>\n" +
                "\n" +
                "<p>\n" +
                "<a name=\"CancelOrder\"><b>CANCELLING AN ORDER</b></a>\n" +
                "<br>Those orders that have not been processed by the NGO can be cancelled before the organisation processes it.\n" +
                "<br><ul><li>STEP 1: Find the Order ID of the order you want to cancel.\n" +
                "        <br><img src=\"cancel.png\"></li>\n" +
                "        <li>STEP 2: Clicking on that ID shows the items in the order.\n" +
                "        <br><img src=\"cancelfull.png\"></li>\n" +
                "        <li>STEP 3: Click on the order and a dialog box appears confirming your action. Click \"<b>OK</b>\".</li>\n" +
                "        <br><img src=\"cancelDialog.png\"></li>\n" +
                "    </ul>\n" +
                "<br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                "</p>\n" +
                "\n" +
                "<p>\n" +
                "<a name=\"Draft\"><b>SAVING A DRAFT</b></a><br>You might want to save the items you would like to order later as drafts. \n" +
                "<br><b>Saving a draft</b>\n" +
                "<br><ul><li>STEP 1: Click on the \"New Order\" button and choose the items and their quantities you want to save.</li>\n" +
                "        <li>STEP 2: Click on \"<b>Draft</b>\" button and a you get the list of items you are saving.\n" +
                "                    <img src=\"draftorder.png\"></li>\n" +
                "        <li>STEP 3: Click on \"<b>Done</b>\" to get your Drafts saved, else clic \"Modify\" to redraft it.</li>\n" +
                "    </ul>\n" +
                "<br><b>Ordering or Deleting a draft</b>\n" +
                "<br><ul><li>STEP 1: Click on the \"<b>Draft Order</b>\" button on the Dashboard. The interface that opens shows your saved drafts.\n" +
                "                    <br><img src=\"draft.png\"></li>\n" +
                "        <li>STEP 2: Choose the items you want to order/delete by checking the boxes.</li>\n" +
                "                    <img src=\"checkeddraft.png\"></li>\n" +
                "        <li>STEP 3: Click on \"<b>Order</b>\" to get your order placed.</li>\n" +
                "        <br> Click on \"<b>Delete</b>\" to delete the draft orders selected.</li>\n" +
                "    </ul>\n" +
                "\n" +
                "\n" +
                "<br><a href=\"#Top\" style=\"text-decoration:none\">Top </a>\n" +
                "</p>\n" +
                "</body>\n" +
                "</html>");





        wv.loadDataWithBaseURL("file:///android_asset/", html.toString(), "text/html", "UTF-8", "");
      //  setContentView(R.layout.activity_help);
       // WebView webView = (WebView)findViewById(R.id.webview);
       // webView.setWebViewClient(new MyBrowser());
    //    webView.loadUrl("file:///android_res/raw/indeex.html");
//        setContentView(webView);
    }
/*
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }*/
}
