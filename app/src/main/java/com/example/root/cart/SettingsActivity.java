package com.example.root.cart;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    public static final String reco_update = "recommend_updates";
    public static final String language = "language";
    public static final String store_orders = "prev_orders";
    public static final String num_orders = "num_orders_stored";
    public static final String sort_orders_by = "orders_sort_by";
    public static final String restore_default = "restore_default";
    public static final String clear_data = "clear_data";
  //  MyDialogPreference restoreDefaultDialog;
    SharedPreferences pref;
    Preference p1,p2;
    SQLiteDatabase db = this.getReadableDatabase();
	OrdersDBHelper dd = new OrdersDBHelper(getBaseContext());
	

    private SQLiteDatabase getReadableDatabase() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        addPreferencesFromResource(R.xml.preferences);

        pref.registerOnSharedPreferenceChangeListener(this);

        p1= findPreference("clear_data_button");
        p1.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(SettingsActivity.this)
                        .setTitle("Clear User Data")
                        .setMessage("Are you sure you want to clear your data for Cart from this device?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                Log.d("cart", "clearing data....");
                               
                            	db= openOrCreateDatabase("Order_drafts.db", SQLiteDatabase.CREATE_IF_NECESSARY, null); 
                               	db.execSQL("Delete from draftss");
                            	
                                
                                
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return false;
            }
        });

        p2 = findPreference("restore_default_button");
        p2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(SettingsActivity.this)
                        .setTitle("Restore Default Settings")
                        .setMessage("Are you sure you want to reset all settings to default?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                Log.d("cart", "restore settings");

                                SharedPreferences sharedPreferences=PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
                                sharedPreferences.edit().remove(store_orders).remove(sort_orders_by).remove(reco_update).remove(language).remove(num_orders).apply();
                                PreferenceManager.setDefaultValues(SettingsActivity.this,R.xml.preferences,true);
                                CheckBoxPreference reco = (CheckBoxPreference) findPreference(reco_update);
                                CheckBoxPreference store = (CheckBoxPreference) findPreference(store_orders);
                                reco.setChecked(true);
                                store.setChecked(true);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return false;
            }
        });

     //   restoreDefaultDialog = (MyDialogPreference) findPreference(restore_default);

        findPreference(language).setSummary(pref.getString(language,""));
        findPreference(num_orders).setSummary(pref.getString(num_orders,""));
        findPreference(sort_orders_by).setSummary(pref.getString(sort_orders_by,""));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_home) {
            Intent i=new Intent(this,DashboardActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        Log.d("cart","pref Listener");
        if(key.equals(language)){
            Preference p = findPreference(key);
            p.setSummary(sharedPreferences.getString(key,""));
        }
        else if(key.equals(sort_orders_by)){
            Preference p = findPreference(key);
            p.setSummary(sharedPreferences.getString(key,""));
        }
        else if(key.equals(num_orders)){
            Preference p = findPreference(key);
            p.setSummary(sharedPreferences.getString(key,""));
        }
        else if(key.equals(clear_data)){

            SwitchPreference p = (SwitchPreference) findPreference(key);
            if(p.isChecked()==true) {

            }
        }
    }
}
