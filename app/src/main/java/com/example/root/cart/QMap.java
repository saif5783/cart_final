package com.example.root.cart;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 1/1/15.
 */
public class QMap {

    Map<String, Integer> quantities;

    public QMap() {
        this.quantities = new HashMap<String, Integer>();
    }


    public void putValue(String key, Integer val) {
        quantities.put(key, val);
    }

    public Integer getValue(String key) {
        return ( quantities.containsKey(key) ? quantities.get(key) : 0 );
    }

    public void printKeySet() {
        for (String k : quantities.keySet()){
            Log.d("cart",k + " " + quantities.get(k));
        }
    }

    static final QMap q=new QMap();
    public static QMap getInstance() {return q;}
};

