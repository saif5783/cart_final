package com.example.root.cart;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;

/**
 * Created by root on 21/1/15.
 */
public class OrderedProductsDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "orders.db";
    public static final String TABLE_NAME = "ordered";
    
    public final class OrderedProductsEntry implements BaseColumns {
    	 public static final String TABLE_NAME = "ordered";
        public static final String COLUMN_NAME_ORDER_ID = "order_id";
        public static final String COLUMN_NAME_ITEM_NAME = "item_name";
        public static final String COLUMN_NAME_QUANTITY = "quantity";
        public static final String COLUMN_NAME_UNIT_PRICE = "unit_price";
        public static final String COLUMN_NAME_ITEM_BILL = "item_bill";

    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + OrderedProductsEntry.TABLE_NAME + " (" +
                    OrderedProductsEntry.COLUMN_NAME_ORDER_ID + " INTEGER NOT NULL, " +
                    OrderedProductsEntry.COLUMN_NAME_ITEM_NAME + " TEXT NOT NULL, " +
                    OrderedProductsEntry.COLUMN_NAME_QUANTITY + " INTEGER NOT NULL, " +
                    OrderedProductsEntry.COLUMN_NAME_UNIT_PRICE + " INTEGER NOT NULL, " +
                    OrderedProductsEntry.COLUMN_NAME_ITEM_BILL + " INTEGER NOT NULL " +
                    ")";

    public static final String SQL_DELETE_ENTRIES =
            " DROP TABLE IF EXISTS " + OrderedProductsEntry.TABLE_NAME;

    public OrderedProductsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + OrderedProductsEntry.TABLE_NAME + " (" +
                OrderedProductsEntry.COLUMN_NAME_ORDER_ID + " INTEGER NOT NULL, " +
                OrderedProductsEntry.COLUMN_NAME_ITEM_NAME + " TEXT NOT NULL, " +
                OrderedProductsEntry.COLUMN_NAME_QUANTITY + " INTEGER NOT NULL, " +
                OrderedProductsEntry.COLUMN_NAME_UNIT_PRICE + " INTEGER NOT NULL, " +
                OrderedProductsEntry.COLUMN_NAME_ITEM_BILL + " INTEGER NOT NULL " +
                ")");
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: Tasks to perform On Upgrde
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public boolean saveProduct(int order_id, String product_name, double quantity, double unit_price, double item_bill )
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OrderedProductsEntry.COLUMN_NAME_ORDER_ID,order_id);
        values.put(OrderedProductsEntry.COLUMN_NAME_ITEM_NAME,product_name);
        values.put(OrderedProductsEntry.COLUMN_NAME_QUANTITY,quantity);
        values.put(OrderedProductsEntry.COLUMN_NAME_UNIT_PRICE,unit_price);
        values.put(OrderedProductsEntry.COLUMN_NAME_ITEM_BILL,item_bill);

        long newRowId = db.insert(OrderedProductsEntry.TABLE_NAME,null,values);
        if(newRowId == -1) return false;
        return true;
    }

    public ArrayList<ItemRow> getOrderedProducts(int order_id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                OrderedProductsEntry.COLUMN_NAME_ITEM_NAME,
                OrderedProductsEntry.COLUMN_NAME_QUANTITY,
                OrderedProductsEntry.COLUMN_NAME_UNIT_PRICE,
                OrderedProductsEntry.COLUMN_NAME_ITEM_BILL
        };

        String sortOrder = OrderedProductsEntry.COLUMN_NAME_ITEM_NAME; // ascending order by default

        String whereClause = OrderedProductsEntry.COLUMN_NAME_ORDER_ID + "= ?";

        String selectionArgs[] = {order_id + ""};

        Cursor c = db.query(OrderedProductsEntry.TABLE_NAME,projection,whereClause,selectionArgs,null,null,sortOrder);

        c.moveToFirst();

        ArrayList<ItemRow> items = new ArrayList<ItemRow>();

        while(!c.isAfterLast())
        {
            items.add(new ItemRow(c.getString(1),c.getDouble(2),c.getDouble(3),c.getDouble(4)));
            c.moveToNext();
        }

        return items;

    }
}
