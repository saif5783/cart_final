package com.example.root.cart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ViewOrderActivity extends Activity implements View.OnClickListener{
    TextView view_orderId,view_orderTime,view_groupName;
    int orderId,groupId;
            String orderTime="";
    Button button_modify,button_cancel;
    HashMap<String,String> itemsMap;
    HashMap<String,String[]> itemList;
    Context context;
    SharedPreferences sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.root.cart.R.layout.activity_view_order);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        itemsMap = new HashMap<String,String>();

        view_orderId =(TextView)findViewById(R.id.label_order_no);
        view_orderTime = (TextView) findViewById(R.id.view_orderTime);
        view_groupName=(TextView) findViewById(R.id.label_group_name);

        //button_back = (Button) findViewById(R.id.button_view_order_back);
        button_modify = (Button) findViewById(R.id.button_view_order_modify);
        button_cancel = (Button) findViewById(R.id.button_view_order_cancel);

        String fontPath1 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface tf1= Typeface.createFromAsset(getAssets(), fontPath1);
        button_modify.setTypeface(tf1);
        button_cancel.setTypeface(tf1);

        //button_back.setOnClickListener(this);
        button_modify.setOnClickListener(this);
        button_cancel.setOnClickListener(this);

        ListView list = (ListView) findViewById(com.example.root.cart.R.id.view_order_item_list);
        List<ItemRow> item_list = getItems();

        view_orderId.setText("Order No: "+orderId);

       // if(groupId==)
        view_groupName.setText("Group Name: "+sharedPref.getString(""+groupId,""));
        view_orderTime.setText("Order Time: "+orderTime);

        ItemRowAdapter adapter = new ItemRowAdapter(this, com.example.root.cart.R.layout.view_order_row, item_list);
        list.setAdapter(adapter);
    }


  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.example.root.cart.R.menu.menu_dashboard, menu);
        return true;
    }
*/
    public List<ItemRow> getItems() {
        List<ItemRow> items = new ArrayList<ItemRow>();

        Bundle extras = getIntent().getExtras();
        orderId=extras.getInt("SavedOrderId");
        groupId=extras.getInt("SavedOrderGroupId");
        Log.d("TimePass VI", orderId + " " + groupId);
        orderTime=extras.getString("SavedOrderTime");
        itemList=(HashMap<String,String[]>)extras.getSerializable("SavedOrderItems");

        for(String itemName : itemList.keySet()){
            String[] temp=itemList.get(itemName);
            double rate=Double.parseDouble(temp[0]);
            double quantity=Double.parseDouble(temp[1]);
            double total = quantity * rate;
            items.add(new ItemRow(itemName,quantity,rate,total));
            itemsMap.put(itemName,rate+"");
        }
        return items;
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == com.example.root.cart.R.id.action_home) {
            Intent i=new Intent(this,DashboardActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/
    @Override
    public void onClick(View view) {

       /* if(view == button_back){
            Intent i=new Intent(getApplicationContext(),PreviousOrdersActivity.class);
            startActivity(i);
            finishViewOrderActivity();
        }*/
        if(view == button_modify){
            Intent i=new Intent(getApplicationContext(),NewOrderActivity.class);
            i.putExtra("itemsMap",itemsMap);
            for(String itemName:itemList.keySet()){
                i.putExtra(itemName,Double.parseDouble(itemList.get(itemName)[1]));
            }
            startActivity(i);
        }
        else if(view == button_cancel){
             finishViewOrderActivity();

        }
    }

    public void finishViewOrderActivity() {
        ViewOrderActivity.this.finish();
    }

}
