package com.example.root.cart;

import android.app.Fragment;
import android.os.Bundle;

/**
 * Created by root on 16/7/15.
 */
public class RetainedFragment extends Fragment {

    // data object we want to retain

    public String getNumber() {
        return number;
    }

    public String getOtp() {
        return otp;
    }

    public String getPwd2() {
        return pwd2;
    }

    public void setPwd2(String pwd2) {
        this.pwd2 = pwd2;
    }

    public String getPwd1() {
        return pwd1;
    }

    public void setPwd1(String pwd1) {
        this.pwd1 = pwd1;
    }

    public long getTimer() {
        return timer;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }

    public void setOtp(String otp) {
        this.otp = otp;

    }

    public void setNumber(String number) {

        this.number = number;
    }

    public long getSession() {
        return session;
    }

    public void setSession(long session) {
        this.session = session;
    }

    private int pos,id;
    private String number,otp,pwd1,pwd2;
    private boolean status,groupDialogFlag;
    private  long timer,session;

    // this method is only called once for this fragment
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return status;
    }

    public void setdialogId(int id) {
        this.id = id;
    }

    public int getdialogId() {
        return id;
    }

    public void setGroupDialogFlag(boolean groupDialogFlag) {
        this.groupDialogFlag = groupDialogFlag;
    }

    public boolean getGroupDialogFlag() {
        return groupDialogFlag;
    }
}
