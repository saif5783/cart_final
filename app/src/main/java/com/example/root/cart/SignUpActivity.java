package com.example.root.cart;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

// implements AdapterView.OnItemSelectedListener
public class SignUpActivity extends Activity {

    EditText emailId,firstPassword,confirmPassword,address,name;

    String memberNumber,response;

    ArrayList<String> grpnames ;
    ArrayList<String> grpids ;
    ArrayList<String> sids ;
    ArrayList<String> snames ;
    ArrayList<ArrayList<String>> subgrpids ;
    ArrayList<ArrayList<String>> subgrpnames ;
    AlertDialog organizationDialog,groupDialog;
    int grpposition,dialogid;
    JSONObject signupObj= new JSONObject();
    JSONArray  org_grp;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    boolean subgrpFlag=false;
    private RetainedFragment dataFragment;
    SubListAdapter sublistAdapter;
    boolean groupDialogFlag;
    SignUpTask signUpTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        setResult(1);
        System.out.println("creatttttttttttttttttedddddddddddddddddddddddddddddddddddddd");

        sharedPref= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPref.edit();


        // System.out.println("grppos--------"+grpposition);





        response = getIntent().getExtras().getString("json");
        memberNumber = getIntent().getExtras().getString("mobilenumber");



        System.out.println("signupppppppp----------->" + response);
        getGroupsDetails();
        // find the retained fragment on activity restarts
        FragmentManager fm = getFragmentManager();
        dataFragment = (RetainedFragment) fm.findFragmentByTag("SignUpActivity");


        if (dataFragment == null) {

            editor.clear();
            editor.commit();
            System.out.println("firstttttttttt timeeeeeeeeeeeeeeeeeeeeee");
            // add the fragment
            dataFragment = new RetainedFragment();
            fm.beginTransaction().add(dataFragment, "SignUpActivity").commit();
            // load the data from the web
            grpposition=-1;
            dialogid=-1;
            groupDialogFlag=false;
            dataFragment.setPos(grpposition);
            dataFragment.setdialogId(dialogid);
            dataFragment.setGroupDialogFlag(groupDialogFlag);
        }

        else
        {
            System.out.println("seconddddddddddddddd timeeeeeeeeeeeeeeeeeeeeee");

            grpposition=dataFragment.getPos();

            if(grpposition!=-1) {
                ((TextView) findViewById(R.id.txtorg)).setText("You selected " + " " + grpnames.get(grpposition));
                ((TextView) findViewById(R.id.txtgrp)).setText("Click here to choose " + " " + grpnames.get(grpposition) + " " + "groups");
            }

            if(dataFragment.getdialogId()==1)
            {
               System.out.println(dataFragment.getdialogId());
               // organizationDialog.show();
                ((TextView)findViewById(R.id.txtorg)).performClick();
            }
            else
            {
                if(dataFragment.getdialogId()==2)
                {
                    groupDialogFlag=dataFragment.getGroupDialogFlag();
                    System.out.println(dataFragment.getdialogId());
                    ((TextView)findViewById(R.id.txtgrp)).performClick();
                   // groupDialog.show();
                }else
                {}

            }


        }







        name=(EditText)findViewById(R.id.name);
        emailId=(EditText)findViewById(R.id.emailID);
        firstPassword=(EditText)findViewById(R.id.firstpassword);
        confirmPassword=(EditText)findViewById(R.id.confirmpassword);
        //contactNumber=(EditText)findViewById(R.id.contactnumber);
       // state=(EditText)findViewById(R.id.state);
       // city=(EditText)findViewById(R.id.city);
        address=(EditText)findViewById(R.id.address);
        //pincode=(EditText)findViewById(R.id.pincode);
}


    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("startedddddddddddddddddddddddddd");
       // editor.clear();
        //editor.commit();
    }


    @Override
    protected void onResume() {
        super.onResume();

        System.out.println("resssssssssssssummmmmmmmmmmmmmeeeeeeeeeee");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("pausedddddddddddddddddddddddddd");

         dataFragment.setPos(grpposition);
       // if(organizationDialog.isShowing())
         dataFragment.setdialogId(dialogid);

      if(dialogid==2)
      {
          for (Subgroup p : sublistAdapter.getBox()) {


              editor.putBoolean("subgrp_pos_status_ondestroy" + grpposition + p.position,p.box);
              editor.commit();
          }

          dataFragment.setGroupDialogFlag(true);
      }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("restarrrrrrrrrrrrrrrrrrtttttttttttttttttttttttt");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("stoooooooooooopppppppppppppppeddddd");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.out.println("killlllllllllllllllllllllllinnnnnnnnnnnnnnnnnnnnngggggggggggggggggggggggggggg");
       /* dataFragment.setPos(grpposition);
       // if(organizationDialog.isShowing())

            dataFragment.setdialogId(dialogid);

      if(dialogid==2)
      {
          for (Subgroup p : sublistAdapter.getBox()) {


              editor.putBoolean("subgrp_pos_status_ondestroy" + grpposition + p.position,p.box);
              editor.commit();
          }

          dataFragment.setGroupDialogFlag(true);
      }*/


     //   if(groupDialog.isShowing())

          //  dataFragment.setdialogId(dialogid);


        //  editor.clear();
         //editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }






    public void getGroupsDetails()
    {

        try {
            JSONObject obj = new JSONObject(response);

            JSONArray groups = obj.getJSONArray("groups");


            for(int i=0;i<groups.length();i++)
            {

                JSONObject grpObj = groups.getJSONObject(i);

                  grpids = new ArrayList<String>();

                  grpnames = new ArrayList<String>();




                subgrpids = new ArrayList<ArrayList<String>>();
                subgrpnames = new ArrayList<ArrayList<String>>();
                //for(int j=0;j<grpObj.length();j++)
             //   {

                    //System.out.println(grpObj.keys().toString());


                    Iterator<String> keys = grpObj.keys();
                    while(keys.hasNext()){
                        String key = keys.next();
                      //  System.out.println(key);
                        String val = null;
                        try{

                            sids = new ArrayList<String>();

                            snames = new ArrayList<String>();
                            JSONArray subgrps = grpObj.getJSONArray(key);
                           // System.out.println("it is json array..............");

                            for(int j=0;j<subgrps.length();j++)
                            {

                                Iterator<String> subkeys = subgrps.getJSONObject(j).keys();
                                while(subkeys.hasNext())
                                {
                                    String subkey = subkeys.next();

                                 //   System.out.println(subkey+ " " +subgrps.getJSONObject(j).getString(subkey));

                                    sids.add(subkey);
                                    snames.add(subgrps.getJSONObject(j).getString(subkey));


                                }


                            }


                            subgrpids.add(sids);
                            subgrpnames.add(snames);



                        }catch(Exception e){
                            val = grpObj.getString(key);

                            grpids.add(key);
                            grpnames.add(val);

                          //  System.out.println(key +" " +val);
                        }


                  //  }



                }



            }







        } catch (JSONException e) {
            e.printStackTrace();
        }



        for(int m =0;m<grpids.size();m++)
        {
            System.out.println(grpids.get(m) + " " +grpnames.get(m));


           // for(int n=0;n<subgrpids.size();n++)
            {

                for(int z=0;z<subgrpids.get(m).size();z++)
                {

                    System.out.println(subgrpids.get(m).get(z)+ " " +subgrpnames.get(m).get(z));

                }

            }




        }







    }


    public void showOrganization(View v)
    {

        dialogid=1;

        final String gnames[] = new String[grpnames.size()];

        for(int k=0;k<grpnames.size();k++)
        {
            gnames[k]=grpnames.get(k);

        }


        AlertDialog.Builder orgDialog = new AlertDialog.Builder(SignUpActivity.this);
        final ListView listview=new ListView(SignUpActivity.this);
        LinearLayout layout = new LinearLayout(SignUpActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);

        layout.addView(listview);
        orgDialog.setView(layout);
        //CustomAlertAdapter arrayAdapter=new CustomAlertAdapter(MainActivity.this, array_sort);
        listview.setAdapter(new ArrayAdapter<String>(SignUpActivity.this,android.R.layout.simple_list_item_1,gnames));
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                  dialogid=-1;
                grpposition = position;

                System.out.println("org -------------------" + grpposition);

                ((TextView)findViewById(R.id.txtorg)).setText("You selected " + " " + gnames[grpposition]);
                ((TextView)findViewById(R.id.txtgrp)).setText("Click here to choose " + " " + gnames[grpposition]+ " "+"groups");

                organizationDialog.dismiss();
                //   String grpname =  parent.getSelectedItem().toString();
                //   SharedPreferences.Editor editor = sharedPref.edit();

                //  editor.putString("groupId", sharedPref.getString(orgname+grpname,""));
                // editor.putString(sharedPref.getString(orgname+grpname,""),grpname);
                //   editor.commit();
                //Toast.makeText(DashboardActivity.this, orgname+"   "+grpname+" "+sharedPref.getString(orgname+grpname,""), Toast.LENGTH_SHORT).show();
            }


        });
/*
        orgDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //dialog.dismiss();
            }
        });

        orgDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
      */
        organizationDialog=orgDialog.show();


    }


    public void showGroup(View v)
    {

        System.out.println("grp -------------------"+grpposition);
        subgrpFlag=false;

        if(grpposition !=-1)
        {

            dialogid=2;

            ArrayList<Subgroup> subgroup = new ArrayList<Subgroup>();

            AlertDialog.Builder grpDialog = new AlertDialog.Builder(SignUpActivity.this);
            final ListView listview=new ListView(SignUpActivity.this);
            LinearLayout layout = new LinearLayout(SignUpActivity.this);
            layout.setOrientation(LinearLayout.VERTICAL);

            layout.addView(listview);
            grpDialog.setView(layout);


            if(groupDialogFlag==true)
            {
               System.out.println("group dialog flag---------------------"+groupDialogFlag);
                for(int i=0;i<subgrpnames.get(grpposition).size();i++)
                {

                     System.out.println(" "+sharedPref.getBoolean("subgrp_pos_status_ondestroy" + grpposition + i,false));

                    subgroup.add(new Subgroup(subgrpnames.get(grpposition).get(i), i, sharedPref.getBoolean("subgrp_pos_status_ondestroy" + grpposition + i, false)));

                }
                groupDialogFlag=false;

            }


          else
            {

                if(sharedPref.getInt("org_subgrpCount" + grpposition,0)!=0)
                {

                    for(int i=0;i<subgrpnames.get(grpposition).size();i++)
                    {
                        // System.out.println(" "+sharedPref.getBoolean("subgrp_pos" + grpposition + i,false));

                        subgroup.add(new Subgroup(subgrpnames.get(grpposition).get(i), i, sharedPref.getBoolean("subgrp_pos_status" + grpposition + i, false)));

                    }




                    System.out.println("total groups already selected-------------------"+sharedPref.getInt("org_subgrpCount" + grpposition,-1));
                    String pos[]= new String[sharedPref.getInt("org_subgrpCount" + grpposition,0)];
                    int subgrpSize = subgrpids.get(grpposition).size();
                    int diff= subgrpSize-sharedPref.getInt("org_subgrpCount" + grpposition,0);

                    for(int p=0;p<sharedPref.getInt("org_subgrpCount" + grpposition,0);p++)
                    {

                        System.out.println(" slected grp ids-------------------"+subgrpids.get(grpposition).get(sharedPref.getInt("subgrp_pos"+p+grpposition,0)));
                        pos[p]=subgrpids.get(grpposition).get(sharedPref.getInt("subgrp_pos"+p+grpposition,0));
                    /*subgroup.add(new Subgroup(
                            subgrpnames.get(grpposition).get(sharedPref.getInt("subgrp_pos"+p+grpposition,-1)),
                            sharedPref.getInt("subgrp_pos"+p+grpposition,-1),
                            true
                    ));*/
                    }

                    //int counter=0;

                    if(diff!=0)
                    {

                        for(int x=0;x<subgrpSize;x++)
                        {
                            int counter=0;
                            for(int m=0;m<pos.length;m++)
                            {
                                if(!subgrpids.get(grpposition).get(x).equals(pos[m]))
                                {

                                    counter++;
                                    //System.out.println(" not slected grp ids-------------------" + subgrpids.get(grpposition).get(x));
                                    // subgroup.add(new Subgroup(subgrpnames.get(grpposition).get(x), x, false));
                                }



                            }


                            if(counter==pos.length) {
                                System.out.println(" not slected grp ids-------------------" + subgrpids.get(grpposition).get(x));
                                // subgroup.add(new Subgroup(subgrpnames.get(grpposition).get(x), x, false));
                            }


                        }


                    }


                }

                else
                {

                    System.out.println("some groups maybe/may not selected -------------------");
                    for(int i=0;i<subgrpnames.get(grpposition).size();i++)
                    {
                        subgroup.add(new Subgroup(subgrpnames.get(grpposition).get(i),i,false));

                    }




                }

            }


          /*  for(int i=0;i<subgrpnames.get(grpposition).size();i++)
            {
                subgroup.add(new Subgroup(subgrpnames.get(grpposition).get(i),i,false));

            }*/



            sublistAdapter = new SubListAdapter(this,subgroup);


            //CustomAlertAdapter arrayAdapter=new CustomAlertAdapter(MainActivity.this, array_sort);
            listview.setAdapter(sublistAdapter);
            // listview.setOnItemSelectedListener(SignUpActivity.this);

            grpDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //dialog.dismiss();

                    dialogid=-1;

                    // editor.putBoolean("registered", true);
                    //  editor.putBoolean("login",true);
                    editor.putString("orgId"+grpposition,grpids.get(grpposition));
                    editor.putString("orgName"+grpposition,grpnames.get(grpposition));
                    //editor.putInt("group",groups[0]);
                    // editor.commit();

                    String result = "Selected Product are :";
                    int count = 0;
                    for (Subgroup p : sublistAdapter.getBox()) {

                        if (p.box) {


                            editor.putString("orgId_subgrpId"+count,subgrpids.get(grpposition).get(p.position));
                            editor.putString("orgId_subgrpname" + count, subgrpnames.get(grpposition).get(p.position));
                            editor.putInt("subgrp_pos" + count + grpposition, p.position);

                            System.out.println(p.position + "  " + subgrpids.get(grpposition).get(p.position) + "  " + subgrpnames.get(grpposition).get(p.position));
                            count++;
                            // editor.putString(subgrpids.get(grpposition).get(p.position),grpids.get(grpposition));
                            // editor.putString("grpName"+grpposition,grpnames.get(grpposition));
                            result += "\n" + p.name + "  "+p.position;

                        }

                        System.out.println("position checked---------->"+ p.box);
                        editor.putBoolean("subgrp_pos_status" + grpposition + p.position,p.box);
                    }

                    editor.putInt("org_subgrpCount" + grpposition, count);

                    editor.commit();

                  //  grpposition =-1;












                    Toast.makeText(SignUpActivity.this, result, Toast.LENGTH_LONG).show();

                }
            });

            grpDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialogid=-1;
                   // grpposition=-1;
                    dialog.dismiss();
                }
            });

            groupDialog=grpDialog.show();





        }

        else
        {


            Toast.makeText(SignUpActivity.this,"Please select the organization", Toast.LENGTH_LONG).show();

        }





    }



    public void signup(View v)
    {

        for(int x =0 ;x<grpids.size();x++)
        {

            if(sharedPref.getInt("org_subgrpCount" + x,0)!=0)
            {

                subgrpFlag=true;


            }

        }

       // String Contactnumber = contactNumber.getText().toString();
        String Username = name.getText().toString();


        String Firstpwd = firstPassword.getText().toString();



        String Confirmpwd = confirmPassword.getText().toString();

        String Emailid = emailId.getText().toString();



/*
        String State = state.getText().toString();

        String City = city.getText().toString();*/




        String Address = address.getText().toString();

      //  String Pincode = pincode.getText().toString();




        if(     Username.equals("") ||
                Firstpwd.equals("")||
                Confirmpwd.equals("") ||
                Emailid.equals("")||
                Address.equals("")||
                grpposition ==-1

        )
        {
            Toast.makeText(getApplicationContext(), "Enter all details", Toast.LENGTH_SHORT).show();
        }
        else
        {



            if(!Emailid.equals(""))
            {

                if(!android.util.Patterns.EMAIL_ADDRESS.matcher(Emailid).matches())
                {
                    Toast.makeText(getApplicationContext(), "Enter valid email id", Toast.LENGTH_SHORT).show();
                }


                else
                {


                    if(!Firstpwd.equals("")&&!Confirmpwd.equals(""))
                    {
                        if(!Firstpwd.equals(Confirmpwd))
                        {
                            Toast.makeText(getApplicationContext(), "Password does not match", Toast.LENGTH_SHORT).show();

                        }
                    else
                        {


                            if(grpposition !=-1 && subgrpFlag==true)
                            {
                                try{
                                    signupObj.put("name",Username);
                                    signupObj.put("email", Emailid);
                                    signupObj.put("password",Firstpwd);
                                    signupObj.put("address", Address);
                                    signupObj.put("number", memberNumber);

                                    //   System.out.println(signupObj.toString());



                                    JSONObject org = new JSONObject();
                                    JSONArray  subgrp;
                                    JSONObject sub;

                                    for(int x =0 ;x<grpids.size();x++)
                                    {
                                        org_grp = new JSONArray();
                                        if(sharedPref.getInt("org_subgrpCount" + x,-1)!=0)

                                        {

                                         //   org.put(sharedPref.getString("orgId"+x,""),sharedPref.getString("orgName"+x,""));
                                            //System.out.println(sharedPref.getString("orgId"+x,""));
                                            //  System.out.println(sharedPref.getString("orgName"+x,""));

                                            subgrp = new JSONArray();

                                            for(int p=0;p<sharedPref.getInt("org_subgrpCount" + x,-1);p++)
                                            {
                                               // sub = new JSONObject();
                                              //  sub.put(sharedPref.getString("orgId_subgrpId"+p,""),sharedPref.getString("orgId_subgrpname"+p,""));
                                                // System.out.println(sharedPref.getString("orgId_subgrpId"+p,""));
                                                // System.out.println(sharedPref.getString("orgId_subgrpname"+p,""));
                                             //   subgrp.put(sub);
                                                subgrp.put(sharedPref.getString("orgId_subgrpId"+p,""));
                                            }

                                            org.put(sharedPref.getString("orgId"+x,""),subgrp);



                                        }

                                        org_grp.put(org);




                                    }

                                    signupObj.put("groups",org_grp);

                                    System.out.println(signupObj.toString());





                                    if(connect()){
                                        signUpTask = new SignUpTask(SignUpActivity.this);
                                        signUpTask.execute(signupObj.toString());
                                    }
                                    else{
                                        Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();

                                    }





                                }catch(JSONException e){
                                    e.printStackTrace();
                                }

                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), "Choose organization and groups", Toast.LENGTH_SHORT).show();
                            }








/*

                            if(!Contactnumber.equals(""))
                            {
                                long i = Long.parseLong(Contactnumber);
                                long length = (long)(Math.log10(i)+1);
                                if(length<10 || length > 10){
                                    Toast.makeText(getApplicationContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
                                }
                                else
                                {


                                    if(!Pincode.equals(""))
                                    {
                                        long j = Long.parseLong(Pincode);
                                        long pinlength = (long)(Math.log10(j)+1);
                                        if(pinlength<6 || pinlength > 6){
                                            Toast.makeText(getApplicationContext(), "Enter valid pincode number", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    else
                                    {
                                        if(connect()){
                                            // loginVerification = new LoginVerification(LoginActivity.this, handler);
                                            //   loginVerification.execute(number);
                                        }
                                        else{
                                            Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();

                                        }
                                    }


                                }

                            }


*/

                        }
                    }






                }
            }






        }


    }



    public class  SignUpTask extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        Context context;

        public SignUpTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
            //  this.handler = handler;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage("Checking number......");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {


            String url =
                    null;
            try {
                url = "http://ruralict.cse.iitb.ac.in/AFC/IvrsServlet?req_type=addNewAppUser&details="+ URLEncoder.encode(params[0], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            JsonParser jParser = new JsonParser();
            Log.d("-------chetan---------","----------------------");
            response = jParser.getJSONFromUrl(url);
            System.out.println("serverrrrrrrrrrrrrrrrrrrrr responsssseeeeeeeeeeeeeeee" +"  "+ response);
            return response;
            //return isPublisher;*/
        }





        @Override
        protected void onPostExecute(String response1) {

            pd.dismiss();


            if(response1.equals("Exception"))
            {
                Log.d("server downnnnnnnnnnnn", response1);


                myDialogFragment ob2=new myDialogFragment();
                //  Dialog ob1=new Dialog(c);


                android.app.FragmentManager fm=getFragmentManager();
                ob2.show(fm, "1");
// Toast.makeText(DashboardActivity.this,"Unable to connect with server",Toast.LENGTH_SHORT).show();



            }
            else
            {

                System.out.println("signnnnnnnnnnnnnnnn uppppppppppppppppppp"+response1);

                try {
                    JSONObject status = new JSONObject(response1);

                    if(status.getString("status").equals("success")) {
                        editor.putString("memberNumber", memberNumber);
                        editor.commit();

                        Toast.makeText(getApplicationContext(), "Sign up successful", Toast.LENGTH_SHORT).show();

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("memberNumber",memberNumber);
                        editor.commit();

                        Intent i = new Intent(SignUpActivity.this, DashboardActivity.class);
                        startActivityForResult(i, 0);
                      //  i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                       // startActivity(i);
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Sorry, please try again later", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }





        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(resultCode)
        {
            case 0:
                setResult(0);
                finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public final class myDialogFragment extends DialogFragment {

        public Dialog onCreateDialog(Bundle savedInstanceState) {


            Log.d("inside", "test");
            System.out.println("inside on create dialog function");
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            System.out.println("builder value is " + builder);
            builder.setMessage("Unable to connect with server. \n Try again later")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //implementation of OK part

                                    finish();

                                }
                            }
                    );
            // Create the AlertDialog object and return it
            return builder.create();
        }


    }



//    public final static boolean isValidEmail(CharSequence target) {
      //  return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
   // }

    public boolean connect(){
        String connection= "false";
        //	ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){

            connection="true";
            //saif return connection;
        }
        //saif else {
        Boolean con = Boolean.valueOf(connection);
        return con;
        //saif}

    }

/*
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        grpposition = i;

        System.out.println("-------------------"+grpposition);

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    */
}
