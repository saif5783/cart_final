package com.example.root.cart;

import android.app.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.lang.Object;
import static java.lang.Boolean.valueOf;


public class LoginActivity extends Activity{


    EditText phoneNumber,password,otp,pass1,pass2;
    String number,otp_check,pwd,pwd1,pwd2,response,response2,response3;
    LoginVerification loginVerification;
    JSONObject checkNumber;
    Dialog dialog;
  //  int dialogid;
    Button getOtp,Confirm,Cancel;
    GetOTPTask anp;
    AddPasswordTask addPasswordTask;
    long session;
    OTPCountDownTimer countDownTimer;
    JSONObject addPasswordDB= new JSONObject();
    SharedPreferences sharedPref;
 //   private RetainedFragment dataFragment;
 //   long timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        phoneNumber=(EditText)findViewById(R.id.phoneNumber);


        setResult(1);
/*

        FragmentManager fm = getFragmentManager();
        dataFragment = (RetainedFragment) fm.findFragmentByTag("LoginActivity");


        if(dataFragment==null)
        {
            System.out.println("firstttttttttt timeeeeeeeeeeeeeeeeeeeeee");
            dataFragment = new RetainedFragment();
            fm.beginTransaction().add(dataFragment, "LoginActivity").commit();

            dialogid=-1;
            dataFragment.setdialogId(dialogid);
            // dialog = new Dialog(MainActivity.this);

        }
        else
        {
            System.out.println("seconddddddddddddddd timeeeeeeeeeeeeeeeeeeeeee");


            number=dataFragment.getNumber();

            if(dataFragment.getdialogId()==1)
            {
                System.out.println(dataFragment.getdialogId());

               // if(otp)
               // number = phoneNumber.getText().toString();

                ((Button)findViewById(R.id.forgot)).performClick();
            }

            else
            {
                if(dataFragment.getdialogId()==2)
                {
                    dialog = new Dialog(LoginActivity.this);
                    dialog.setContentView(R.layout.new_password_box);
                    dialog.setTitle("Set New Password");
                    dialog.setCanceledOnTouchOutside(false);

                    otp = (EditText) dialog.findViewById(R.id.enterOTP);
                    // otp.setVisibility(View.GONE);

                    pass1=(EditText) dialog.findViewById(R.id.Password1);
                    // pass1.setVisibility(View.GONE);

                    pass2=(EditText) dialog.findViewById(R.id.Password2);
                    //  pass2.setVisibility(View.GONE);

                    Confirm = (Button)dialog. findViewById(R.id.buttonConfirm);
                    // Confirm.setVisibility(View.GONE);

                    getOtp = (Button)dialog. findViewById(R.id.getOTP);
                    Cancel = (Button)dialog. findViewById(R.id.cancel);

                    otp.setVisibility(View.VISIBLE);
                    pass1.setVisibility(View.VISIBLE);
                    pass2.setVisibility(View.VISIBLE);;
                    getOtp.setVisibility(View.GONE);
                    Confirm.setVisibility(View.VISIBLE);
                    Cancel.setVisibility(View.GONE);
                    session=dataFragment.getSession();
                    countDownTimer = new OTPCountDownTimer(dataFragment.getTimer(), 1000);
                    countDownTimer.start();
                    if(!dataFragment.getOtp().equals(""))
                        otp.setText(dataFragment.getOtp());
                    if(!dataFragment.getPwd1().equals(""))
                        pass1.setText(dataFragment.getPwd1());
                    if(!dataFragment.getPwd2().equals(""))
                        pass2.setText(dataFragment.getPwd2());



                    Confirm.setOnClickListener(new View.OnClickListener()


                                               {
                                                   @Override
                                                   public void onClick(View v) {

                                                       long session2 = System.currentTimeMillis();
                                                       if (session2 < session) {






                                                           if (otp.getText().toString().equals(otp_check)) {
                                                               countDownTimer.cancel();
                                                               pwd1 = pass1.getText().toString();
                                                               pwd2 = pass2.getText().toString();
                                                               if (pwd1.equals(pwd2)) {
                                                                   //sending password to server and updating database
                                                                   try {
                                                                       addPasswordDB.put("number", number);
                                                                       addPasswordDB.put("password", pwd1);
                                                                       if (connect()) {
                                                                           addPasswordTask = new AddPasswordTask(LoginActivity.this);
                                                                           addPasswordTask.execute(addPasswordDB.toString());
                                                                       } else {
                                                                           Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();

                                                                       }

                                                                   } catch (JSONException e) {
                                                                       e.printStackTrace();
                                                                   }

                                                               } else {
                                                                   pass1.setText("");
                                                                   pass2.setText("");
                                                                   Toast.makeText(getApplicationContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();

                                                               }

                                                           } else {
                                                               otp.setHint("Enter the OTP recieved");
                                                               // otp.setText(null);
                                                               Toast.makeText(getApplicationContext(), "Please enter correct OTP", Toast.LENGTH_SHORT).show();

                                                           }
                                                       } else {
                                                           otp.setText("");
                                                           otp.setHint("Enter the OTP recieved");
                                                           otp.setEnabled(false);

                                                           pass1.setText("");
                                                           pass1.setHint("Enter new password");
                                                           pass1.setEnabled(false);

                                                           pass2.setText("");
                                                           pass2.setHint("Confirm password");
                                                           pass2.setEnabled(false);

                                                           getOtp.setEnabled(true);
                                                           Confirm.setEnabled(false);

                                                           Toast.makeText(getApplicationContext(), "Sorry,your OTP expires.Try to get new OTP", Toast.LENGTH_SHORT).show();

                                                       }


                                                   }

                                                   // Perform button logic
                                               }
                    );









                    dialog.show();

                }

            }

        }
*/


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.example.root.cart.R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case com.example.root.cart.R.id.action_home:
                return true;
            case com.example.root.cart.R.id.action_settings:
                Intent intent = new Intent(this,OverflowActivity.class);
                startActivity(intent);
                return true;
            case com.example.root.cart.R.id.action_logout:{
                sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.clear();
                editor.commit();
                finish();
            }
            /*case R.id.action_delete:{
                myDialogFragment2 ob=new myDialogFragment2();
                //  Dialog ob1=new Dialog(c);


                android.app.FragmentManager fm=getFragmentManager();
                ob.show(fm, "1");
            }*/

            default:
                return super.onOptionsItemSelected(item);
        }

        //return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("startedddddddddddddddddddddddddd");
        // editor.clear();
        //editor.commit();
    }


    @Override
    protected void onResume() {
        super.onResume();

        System.out.println("resssssssssssssummmmmmmmmmmmmmeeeeeeeeeee");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("pausedddddddddddddddddddddddddd");

     /*   if(dialogid==1)
        {
            System.out.println("dialogid1111111111111111111"+dialogid);
            dataFragment.setdialogId(dialogid);

        }
        if(dialogid==2)
        {
            System.out.println("dialogid2222222222222222" + dialogid);
            dataFragment.setdialogId(dialogid);
            if(otp.getText().toString().equals(""))
                dataFragment.setOtp("");
            else
            dataFragment.setOtp(otp.getText().toString());

            if(pass1.getText().toString().equals(""))
                dataFragment.setPwd1("");
            else
                dataFragment.setPwd1(pass1.getText().toString());

            if(pass2.getText().toString().equals(""))
                dataFragment.setPwd2("");
            else
                dataFragment.setPwd2(pass2.getText().toString());

          //  dataFragment.setPwd1(pwd1);
        //    dataFragment.setPwd2(pwd2);
            dataFragment.setTimer(timer);
            dataFragment.setSession(session);
        }
        dataFragment.setNumber(number);
*/

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("restarrrrrrrrrrrrrrrrrrtttttttttttttttttttttttt");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("stoooooooooooopppppppppppppppeddddd");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.out.println("killlllllllllllllllllllllllinnnnnnnnnnnnnnnnnnnnngggggggggggggggggggggggggggg");

        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();

       /* dataFragment.setPos(grpposition);
       // if(organizationDialog.isShowing())

            dataFragment.setdialogId(dialogid);

      if(dialogid==2)
      {
          for (Subgroup p : sublistAdapter.getBox()) {


              editor.putBoolean("subgrp_pos_status_ondestroy" + grpposition + p.position,p.box);
              editor.commit();
          }

          dataFragment.setGroupDialogFlag(true);
      }*/


        //   if(groupDialog.isShowing())

        //  dataFragment.setdialogId(dialogid);


        //  editor.clear();
        //editor.commit();
    }




        public void login(View v){


            number = phoneNumber.getText().toString();

            password=(EditText)findViewById(R.id.password);
            pwd = password.getText().toString();


            if(number.equals(""))
            {
                Toast.makeText(getApplicationContext(), "Enter number", Toast.LENGTH_SHORT).show();
            }
            else
            {
                long i = Long.parseLong(number);
                long length = (long)(Math.log10(i)+1);
                if(length<10 || length > 10){
                    Toast.makeText(getApplicationContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(connect()){
                        loginVerification = new LoginVerification(LoginActivity.this);
                        loginVerification.execute(number,pwd);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();

                    }
                }
            }

        }

       /* sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        registered = sharedPref.getBoolean("registered", false);
        isPublisher = Boolean.valueOf(sharedPref.getString("publisher", "false"));
*/


       /* if(registered){

            editor.putBoolean("login", false);
            editor.commit();
            Intent i2 = new Intent(LoginActivity.this,DashboardActivity.class);
            startActivity(i2);

        }

        else
        {
            editor.putBoolean("login", true);
            editor.commit();

            String fontPath3 ="fonts/Caviar_Dreams_Bold.ttf";
            Typeface tf3 = Typeface.createFromAsset(getAssets(), fontPath3);

            setContentView(R.layout.activity_sign_in);
            Button login = (Button)findViewById(R.id.Login);
            login.setTypeface(tf3);
            phoneNumber = (EditText)findViewById(com.example.root.cart.R.id.phoneNumber);
            password = (EditText)findViewById(R.id.password);
        }*/




    //----------------------------------------------------------------------------------------


    public class LoginVerification extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        Context context;
       // Handler handler;

        public LoginVerification(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
          //  this.handler = handler;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage("Checking credentials......");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String url1 =
                    "http://ruralict.cse.iitb.ac.in/AFC/IvrsServlet?req_type=authorising&number="
                            + params[0] + "&pwd=" + params[1];
            JsonParser jParser = new JsonParser();
            Log.d("-------saif---------", "----------------------");
            response = jParser.getJSONFromUrl(url1);
            return response;

        }

        @Override
        protected void onPostExecute(String response) {

            pd.dismiss();


            if (response.equals("exception")) {
                Log.d("server downnnnnnnnnnnn", response);
                myDialogFragment ob2 = new myDialogFragment();
                android.app.FragmentManager fm = getFragmentManager();
                ob2.show(fm, "1");
            } else {
                Log.d("server okkkkkkkkkkk", response);

                try {
                    checkNumber = new JSONObject(response);
                } catch (JSONException e) {
                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                }

                try {
                    if (checkNumber.get("registered").toString().equals("true")) {

                        Log.d("test", checkNumber.get("registered").toString());
                        try {
                            String passCheck = checkNumber.get("password").toString();
                            if (passCheck.equals("correct")) {

                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putString("memberNumber",number);
                                editor.commit();

                                Intent i = new Intent(LoginActivity.this, DashboardActivity.class);
                                startActivityForResult(i, 0);
                               // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK );

                              //  startActivity(i);
                              //  finishActivity();
                                //finish();
                            } else if (passCheck.equals("incorrect")) {
                                password.setText("");
                                Toast.makeText(getApplicationContext(), "Incorrect password", Toast.LENGTH_SHORT).show();
                            } else if (passCheck.equals("null")) {
                                Toast.makeText(getApplicationContext(), "Your password is not set.\nPlease Click on 'Forgot Password' button", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "You are not a registered member", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        startActivityForResult(i, 1);
                       // startActivity(i2);
                       // finish();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block

                    e.printStackTrace();
                }


            }

        }
    }


    //------------------------------------------------------------------------------------------

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(resultCode)
        {
            case 0:
                setResult(0);
                finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    public void forgot(View view)   {


        System.out.println("ffffffooooooooooorrrrrrrrrrrrgggggggggggggggggggotttttttttttttttttttt");

       // if(dialogid==-1)
        {
            number = phoneNumber.getText().toString();
        }

       // dialogid=1;


       /// password=(EditText)findViewById(R.id.password);
       // pwd = password.getText().toString();


        if(number.equals(""))
        {
            Toast.makeText(getApplicationContext(), "Enter number", Toast.LENGTH_SHORT).show();
        }
        else {
            long i = Long.parseLong(number);
            long length = (long) (Math.log10(i) + 1);
            if (length < 10 || length > 10) {
                Toast.makeText(getApplicationContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
            } else {

                if(connect())
                {

                    //  AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                    dialog = new Dialog(LoginActivity.this);
                    dialog.setContentView(R.layout.new_password_box);
                    dialog.setTitle("Verification");
                    dialog.setCanceledOnTouchOutside(false);
                    // set the custom dialog components - text, image and button
                    otp = (EditText) dialog.findViewById(R.id.enterOTP);
                    otp.setVisibility(View.GONE);

                    pass1=(EditText) dialog.findViewById(R.id.Password1);
                    pass1.setVisibility(View.GONE);

                    pass2=(EditText) dialog.findViewById(R.id.Password2);
                    pass2.setVisibility(View.GONE);

                    Confirm = (Button)dialog. findViewById(R.id.buttonConfirm);
                    Confirm.setVisibility(View.GONE);

                    getOtp = (Button)dialog. findViewById(R.id.getOTP);
                    Cancel = (Button)dialog. findViewById(R.id.cancel);

                    Cancel.setOnClickListener(new View.OnClickListener()


                                              {
                                                  @Override
                                                  public void onClick(View v) {

                                                     dialog.dismiss();

                                                  }

                                                  // Perform button logic
                                              }
                    );

                    getOtp.setOnClickListener(new View.OnClickListener()


                                              {
                                                  @Override
                                                  public void onClick(View v) {

                                                      anp = new GetOTPTask(LoginActivity.this);
                                                      anp.execute(number);

                                                  }

                                                  // Perform button logic
                                              }
                    );

                    Confirm.setOnClickListener(new View.OnClickListener()


                                               {
                                                   @Override
                                                   public void onClick(View v) {

                                                       long session2 = System.currentTimeMillis();
                                                       if(session2<session)
                                                       {
                                                           if(otp.getText().toString().equals(otp_check))
                                                           {
                                                               countDownTimer.cancel();
                                                               pwd1 = pass1.getText().toString();
                                                               pwd2 = pass2.getText().toString();
                                                               if(pwd1.equals(pwd2)){
                                                                   //sending password to server and updating database
                                                                   try{
                                                                       addPasswordDB.put("number",number);
                                                                       addPasswordDB.put("password",pwd1);
                                                                       if(connect()){
                                                                           addPasswordTask = new AddPasswordTask(LoginActivity.this);
                                                                           addPasswordTask.execute(addPasswordDB.toString());
                                                                       }
                                                                       else{
                                                                           Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();

                                                                       }

                                                                   }catch(JSONException e){
                                                                       e.printStackTrace();
                                                                   }

                                                               }
                                                               else {
                                                                   pass1.setText("");
                                                                   pass2.setText("");
                                                                   Toast.makeText(getApplicationContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();

                                                               }

                                                           }
                                                           else
                                                           {
                                                               otp.setHint("Enter the OTP recieved");
                                                               // otp.setText(null);
                                                               Toast.makeText(getApplicationContext(), "Please enter correct OTP", Toast.LENGTH_SHORT).show();

                                                           }
                                                       }
                                                       else
                                                       {
                                                           otp.setText("");
                                                           otp.setHint("Enter the OTP recieved");
                                                           otp.setEnabled(false);

                                                           pass1.setText("");
                                                           pass1.setHint("Enter new password");
                                                           pass1.setEnabled(false);

                                                           pass2.setText("");
                                                           pass2.setHint("Confirm password");
                                                           pass2.setEnabled(false);

                                                           getOtp.setEnabled(true);
                                                           Confirm.setEnabled(false);

                                                           Toast.makeText(getApplicationContext(), "Sorry,your OTP expires.Try to get new OTP", Toast.LENGTH_SHORT).show();

                                                       }


                                                   }

                                                   // Perform button logic
                                               }
                    );


                    dialog.show();


           /* i =  new Intent(MainActivity.this,SignUpActivity.class);
            startActivity(i);*/
                    //  finish();
                }
                else
                {
                    Toast.makeText(this,"Please check WIFI/Mobile data connection",Toast.LENGTH_SHORT).show();
                }

                /*if (connect()) {
                    loginVerification = new LoginVerification(LoginActivity.this);
                    loginVerification.execute(number, pwd);
                    }
                else {
                    Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();

                }*/
            }
        }

    }

        //-----------------------------------------------------------------------------------------------

        public class GetOTPTask extends AsyncTask<String, String, String> {

            ProgressDialog pd;
            Context context;

            public GetOTPTask(Context context) {
                // TODO Auto-generated constructor stub
                this.context = context;
                //  this.handler = handler;
            }


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
               // lockScreenOrientation();
                pd = new ProgressDialog(context);
                pd.setMessage("Sending OTP......");
                pd.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String url =
                        "http://ruralict.cse.iitb.ac.in/AFC/IvrsServlet?req_type=getOTP&number="
                                +params[0]+"";

                JsonParser jParser = new JsonParser();
                Log.d("-------chetan---------","----------------------");
                response2 = jParser.getJSONFromUrl(url);


                return response2;
                //return isPublisher;*/
            }





            @Override
            protected void onPostExecute(String response2) {

                pd.dismiss();


                if(response2.equals("exception"))
                {
                    Log.d("server downnnnnnnnnnnn", response2);

                    myDialogFragment ob2=new myDialogFragment();
                    android.app.FragmentManager fm=getFragmentManager();
                    ob2.show(fm, "1");
                }
                else
                {

                    System.out.println("loginnnnnnnnnnnactivittttyyyyyyyyyyyyyyyyyy"+response2);


                    try {
                        JSONObject obj = new JSONObject(response2);

                        otp_check = obj.getString("otp");

                        if(otp_check.equals("null")){
                            Toast.makeText(getApplicationContext(), "You are not a registered member", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else {
                          //  dialogid = 2;
                            dialog.setTitle("Set New Password");
                            otp.setVisibility(View.VISIBLE);
                            pass1.setVisibility(View.VISIBLE);
                            pass2.setVisibility(View.VISIBLE);;
                            getOtp.setVisibility(View.GONE);
                            Confirm.setVisibility(View.VISIBLE);
                            Cancel.setVisibility(View.GONE);
                            session = System.currentTimeMillis();
                            countDownTimer = new OTPCountDownTimer(60000, 1000);
                            countDownTimer.start();
                            session = session + 60000;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
             //   unlockScreenOrientation();




            }
        }

        private void lockScreenOrientation() {
            int currentOrientation = getResources().getConfiguration().orientation;
            if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }

        private void unlockScreenOrientation() {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }
    // CountDownTimer class
    public class OTPCountDownTimer extends CountDownTimer
    {

        public OTPCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
            otp.setText("");
            otp.setHint("Enter the OTP recieved");
            otp.setEnabled(false);
            getOtp.setEnabled(true);
            Toast.makeText(getApplicationContext(), "Sorry,your OTP expires.Try to get new OTP", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onTick(long millisUntilFinished)
        {

            //timer=millisUntilFinished;
        }
    }
    //---------------------------------------------------
    //sends password and number to server and updates Password field of member_details table in DB

    public class  AddPasswordTask extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        Context context;

        public AddPasswordTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
            //  this.handler = handler;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage("Saving your new password......");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {


            String url = null;
            try {
                url = "http://ruralict.cse.iitb.ac.in/AFC/IvrsServlet?req_type=addNewPassword&details="+ URLEncoder.encode(params[0], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            JsonParser jParser = new JsonParser();
            Log.d("-------saifur---------", "----------------------");
            response3 = jParser.getJSONFromUrl(url);
            System.out.println("serverrrrrrrrrrrrrrrrrrrrr responsssseeeeeeeeeeeeeeee" + "  " + response3);
            return response3;
            //return isPublisher;*/
        }





        @Override
        protected void onPostExecute(String response3) {

            pd.dismiss();


            if(response3.equals("exception"))
            {
                Log.d("server downnnnnnnnnnnn", response3);


                myDialogFragment ob2=new myDialogFragment();
                //  Dialog ob1=new Dialog(c);


                android.app.FragmentManager fm=getFragmentManager();
                ob2.show(fm, "1");
// Toast.makeText(DashboardActivity.this,"Unable to connect with server",Toast.LENGTH_SHORT).show();



            }
            else
            {
                try {
                    JSONObject obj = new JSONObject(response3);
                    String msg = obj.getString("status");


                System.out.println("signnnnnnnnnnnnnnnn uppppppppppppppppppp"+response3);

                if(msg.equals("success")) {
                    Toast.makeText(getApplicationContext(), "Password successfully changed", Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(getApplicationContext(), "Sorry, please try again later", Toast.LENGTH_SHORT).show();
                    finish();
                }
                    dialog.dismiss();
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }





        }
    }




    //-----------------------------------------

    public final class myDialogFragment extends DialogFragment {

        public Dialog onCreateDialog(Bundle savedInstanceState) {


            Log.d("inside", "test");
            System.out.println("inside on create dialog function");
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            System.out.println("builder value is " + builder);
            builder.setMessage("Unable to connect with server. \n Try again later")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //implementation of OK part

                                    finish();

                                }
                            }
                    );
            // Create the AlertDialog object and return it
            return builder.create();
        }

    }


    public boolean connect(){
        String connection= "false";
        //	ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){

            connection="true";
            //saif return connection;
        }
        //saif else {
        Boolean con = Boolean.valueOf(connection);
        return con;
        //saif}

    }
   /* @Override
    protected void onDestroy() {
        super.onDestroy();
    }
*/

}
