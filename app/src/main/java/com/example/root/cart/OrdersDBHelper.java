package com.example.root.cart;

import android.content.ContentValues;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by root on 21/1/15.
 */
public class OrdersDBHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "orders.db";

    public final class OrdersEntry implements BaseColumns{
        public static final String TABLE_NAME = "orders";
        public static final String COLUMN_NAME_ORDER_ID = "order_id";
        public static final String COLUMN_NAME_ORDER_REFERENCE_NO = "order_ref";
        public static final String COLUMN_NAME_MEMBER_NUMBER = "member_num";
        public static final String COLUMN_NAME_GROUP_ID = "group_id";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_STATUS = "status";

    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + OrdersEntry.TABLE_NAME + " (" +
            OrdersEntry.COLUMN_NAME_ORDER_ID + " INTEGER PRIMARY KEY, " +
            OrdersEntry.COLUMN_NAME_ORDER_REFERENCE_NO + " INTEGER NOT NULL, " +
            OrdersEntry.COLUMN_NAME_MEMBER_NUMBER + " TEXT NOT NULL, " +
            OrdersEntry.COLUMN_NAME_GROUP_ID + " INTEGER NOT NULL, " +
            OrdersEntry.COLUMN_NAME_TIMESTAMP + " TEXT NOT NULL, " +
            OrdersEntry.COLUMN_NAME_STATUS + " TEXT NOT NULL" +
            ")";

    public static final String SQL_DELETE_ENTRIES =
            " DROP TABLE IF EXISTS " + OrdersEntry.TABLE_NAME;

    public OrdersDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
        db.execSQL(OrderedProductsDBHelper.SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: Tasks to perform On Upgrde
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long getReferenceNumber() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT MAX(" + OrdersEntry.COLUMN_NAME_ORDER_REFERENCE_NO + ") FROM " + OrdersEntry.TABLE_NAME ,null);
        long ref_num;
        if(c.getCount()>0){
            Log.d("Cursor", c.getCount()+"");
            c.moveToFirst();
            ref_num= c.getInt(0);
        }
        else{
            ref_num = 1;
        }
        c.close();
        db.close();
        return (ref_num+1);
    }

    public long insertOrder(int orderNumber, int groupId, String memberNumber, long orderRefNum) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OrdersEntry.COLUMN_NAME_ORDER_ID, orderNumber);
        values.put(OrdersEntry.COLUMN_NAME_ORDER_REFERENCE_NO, orderRefNum);
        values.put(OrdersEntry.COLUMN_NAME_GROUP_ID, groupId);
        values.put(OrdersEntry.COLUMN_NAME_MEMBER_NUMBER, memberNumber);
        values.put(OrdersEntry.COLUMN_NAME_TIMESTAMP, "datetime(\'YYYY-MM-DD HH:MM:SS\')");
        values.put(OrdersEntry.COLUMN_NAME_STATUS, "saved");

        long newRowID = db.insert(OrdersEntry.TABLE_NAME, null, values);

        return newRowID;
    }

    public ArrayList<SavedOrder> getOrdersByNumber(String memberNumber){

        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                OrdersEntry.COLUMN_NAME_MEMBER_NUMBER,
                OrdersEntry.COLUMN_NAME_ORDER_REFERENCE_NO,
                OrdersEntry.COLUMN_NAME_ORDER_ID,
                OrdersEntry.COLUMN_NAME_GROUP_ID,
                OrdersEntry.COLUMN_NAME_TIMESTAMP,
                OrdersEntry.COLUMN_NAME_STATUS
        };

        String sortOrder = OrdersEntry.COLUMN_NAME_ORDER_REFERENCE_NO; // ascending order by default

        String whereClause = OrdersEntry.COLUMN_NAME_MEMBER_NUMBER + "= ?";

        String selectionArgs[] = {memberNumber};

        Cursor c = db.query(OrdersEntry.TABLE_NAME,projection,whereClause,selectionArgs,null,null,sortOrder);

        c.moveToFirst();

        ArrayList<SavedOrder> orders = new ArrayList<SavedOrder>();

        while(!c.isAfterLast())
        {
            orders.add(new SavedOrder(c));
            c.moveToNext();
        }

        return orders;
    }
}
