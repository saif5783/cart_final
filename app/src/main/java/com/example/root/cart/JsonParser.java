package com.example.root.cart;

/**
 * Created by root on 31/12/14.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JsonParser {
    static InputStream is = null;
    static JSONObject jObj = null;
    String status;
    // constructor
    public JsonParser() {
    }
   /* public JSONObject getJSONFromUrl(String url) {
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        // return JSON String
        return jObj;
    }*/

    public String getJSONFromUrl(String url) {

        status="exception";
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            status=String.valueOf(httpResponse.getStatusLine().getStatusCode());

            System.out.println("noooooooooooooooooooooo exceptionnnnnnnnnnnnnnnnn"+"  "+status);
           // System.out.println("status code" + status);

            // Log.d("status code",httpResponse.getStatusLine().getStatusCode()+"");
            //Log.d("status line",httpResponse.getStatusLine()+"");

            if(status.equals("200"))
            {
                System.out.println("serverrrrrrrrrrrrrrr  statussssssssssssss okkkkkkkkkk "+"  "+status);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }
            else
            {
                status="exception";
                System.out.println("serverrrrrrrrrrrrrrrr probblllllllllemmmmmmmmmm"+"  "+status);
                return  status;
            }


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            status="exception";
            System.out.println("exxxxxxxxxxxxxcceppppppptiiiionnnnnnnnnnnn"+"  "+status);
            return status;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            status="exception";
            System.out.println("exxxxxxxxxxxxxcceppppppptiiiionnnnnnnnnnnn" +"  "+status);
            return status;
        } catch (IOException e) {
            e.printStackTrace();
            status="exception";
            System.out.println("exxxxxxxxxxxxxcceppppppptiiiionnnnnnnnnnnn"+"  "+ status);
            return status;
        }


        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            status = sb.toString();
            System.out.println(status);

            try {
                new JSONObject(status);

                System.out.println("innnnnnnnppppppppppputttttttttt strtttttttttttttreammmmmmmm" +"  "+ status);
                return status;
            } catch (Exception e) {
                // return false;
                status="exception";
                System.out.println("innnnnnnnppppppppppputttttttttt strtttttttttttttreammmmmmmm" +"  "+ status);
                return status;
            }

            /*Object json = (Object)status;
            if(json instanceof JSONObject){
                System.out.println("innnnnnnnppppppppppputttttttttt strtttttttttttttreammmmmmmm" +"  "+ status);
                return status;
            }
            else{
                status="exception";
                return status;
            }*/
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        return status;
    }
    public static Map toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

}
