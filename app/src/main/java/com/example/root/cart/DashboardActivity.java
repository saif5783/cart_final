package com.example.root.cart;

import android.app.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.lang.Object;
import static java.lang.Boolean.valueOf;


public class DashboardActivity extends Activity implements OnClickListener , OnItemSelectedListener{


    Button newOrderButton, previousOrderButton,draftButton,orderCancelButton;
    // Button settingButton;
    TextView newtv,newim, prevtv,previm, canceltv,canim, drafttv,draftim;
    LoginVerification loginVerification;
    String memberNumber="";
    SharedPreferences sharedPref;
    Spinner group_spinner , subgroup_spinner;
    ArrayAdapter<String>adapter_state;
    JSONArray org;
    String groupid,grpname;
    ArrayList<String> org_list = new ArrayList<>();
    ArrayList<ArrayList<String>> subgrup_list = new ArrayList<>();
    ArrayList<String> group_list = new ArrayList<>();
    String org_name, response3;
    String orgList[];
    //DeleteAccount deleteAccount;
   // boolean login;
    JSONObject checkNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.root.cart.R.layout.activity_dashboard);

        setResult(0);

        newOrderButton = (Button) findViewById(com.example.root.cart.R.id.button_new_order);
        newtv = (TextView) findViewById(R.id.tv_new);
        newim = (TextView) findViewById(R.id.tvim_new);

        previousOrderButton = (Button) findViewById(com.example.root.cart.R.id.button_prev_orders);
        prevtv = (TextView) findViewById(R.id.tv_prev);
        previm = (TextView) findViewById(R.id.tvim_prev);

        draftButton = (Button) findViewById(com.example.root.cart.R.id.button_help);
        drafttv = (TextView) findViewById(R.id.tv_draft);
        draftim = (TextView) findViewById(R.id.tvim_draft);

        //settingButton=(Button)findViewById(com.example.root.cart.R.id.button_settings);
        orderCancelButton = (Button) findViewById(com.example.root.cart.R.id.button_order_cancel);
        canceltv = (TextView) findViewById(R.id.tv_cancel);
        canim = (TextView) findViewById(R.id.tvim_can);

        group_spinner = (Spinner) findViewById(com.example.root.cart.R.id.group_spinner);
        subgroup_spinner = (Spinner) findViewById(com.example.root.cart.R.id.subgroup_spinner);
        String fontPath = "fonts/Capture_it.ttf";
        String fontPath2 = "fonts/CaviarDreams.ttf";

        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);


        group_spinner.setOnItemSelectedListener(DashboardActivity.this);
        // subgroup_spinner.setOnItemSelectedListener(this);

        newOrderButton.setTypeface(tf);
        previousOrderButton.setTypeface(tf);
        draftButton.setTypeface(tf);
        //settingButton.setTypeface(tf);
        orderCancelButton.setTypeface(tf);

        newtv.setTypeface(tf2);
        prevtv.setTypeface(tf2);
        canceltv.setTypeface(tf2);
        drafttv.setTypeface(tf2);

        newOrderButton.setOnClickListener(DashboardActivity.this);
        previousOrderButton.setOnClickListener(DashboardActivity.this);
        draftButton.setOnClickListener(DashboardActivity.this);
        // settingButton.setOnClickListener(DashboardActivity.this);
        orderCancelButton.setOnClickListener(DashboardActivity.this);
        newtv.setOnClickListener(DashboardActivity.this);
        newim.setOnClickListener(DashboardActivity.this);
        prevtv.setOnClickListener(DashboardActivity.this);
        previm.setOnClickListener(DashboardActivity.this);
        drafttv.setOnClickListener(DashboardActivity.this);
        draftim.setOnClickListener(DashboardActivity.this);
        canceltv.setOnClickListener(DashboardActivity.this);
        canim.setOnClickListener(DashboardActivity.this);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        memberNumber = sharedPref.getString("memberNumber", "");
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("login",true);
        editor.commit();

        /*loginVerification = new LoginVerification(DashboardActivity.this);
        loginVerification.execute(memberNumber);*/

        }


    //------------------------------------------------------------------------------------------

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(resultCode)
        {
            case 0:
                setResult(0);
                finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.example.root.cart.R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
       
        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case com.example.root.cart.R.id.action_home:
                return true;
            case com.example.root.cart.R.id.action_settings:
            	Intent intent = new Intent(this,OverflowActivity.class);
            	startActivity(intent);
                return true;
            case com.example.root.cart.R.id.action_logout:{
                sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.clear();
                editor.commit();
                finish();
            }
            /*case R.id.action_delete:{
                myDialogFragment2 ob=new myDialogFragment2();
                //  Dialog ob1=new Dialog(c);


                android.app.FragmentManager fm=getFragmentManager();
                ob.show(fm, "1");
            }*/

            default:
                return super.onOptionsItemSelected(item);
        }

        //return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
    	// for internet connectivity
       /*saif ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
   	   NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
   	   NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);*/
    	
   	   if(view== newOrderButton || view==newtv || view==newim){
   		//saif if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
        	if(connect()){
            Intent i = new Intent(this,NewOrderActivity.class);
            startActivity(i);
        	}
        	else{
        		
        		Toast.makeText(DashboardActivity.this,"No internet connection", Toast.LENGTH_LONG).show();
        		Intent i = new Intent(this,DashboardActivity.class);
                startActivity(i);
        		
        	}
        }
        else if(view ==  previousOrderButton || view==prevtv || view==previm){
        	
        	//saif if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
            	if(connect()){
        		 Intent i = new Intent(this,PreviousOrdersActivity.class);
                 startActivity(i);
            	}
            	else{
            		
            		Toast.makeText(DashboardActivity.this,"No internet connection", Toast.LENGTH_LONG).show();
            		Intent i = new Intent(this,DashboardActivity.class);
                    startActivity(i);
            		
            	}
           
        }
        else if(view == draftButton || view==drafttv || view==draftim){
            Intent i = new Intent(this,draftActivity.class);
           startActivity(i);
        }
       /* else if(view == settingButton){
            Intent i = new Intent(this,SettingsActivity.class);
            startActivity(i);
        }*/
 
        else if(view == orderCancelButton || view==canceltv || view==canim){
        	//saif if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
           if(connect()){
        		Intent i = new Intent(this,CancelOrder.class);
                startActivity(i);
        	}
        	else{
        		Toast.makeText(DashboardActivity.this,"No internet connection", Toast.LENGTH_LONG).show();
        		Intent i = new Intent(this,DashboardActivity.class);
                startActivity(i);
        	}
        
        }

    }

    //----------------------------------------------------------------------------------------


    public class LoginVerification extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        Context context;

        public LoginVerification(Context context){
            // TODO Auto-generated constructor stub
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage("Loading data......");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String url1 =
                    "http://ruralict.cse.iitb.ac.in/AFC/IvrsServlet?req_type=memberGroups&number="
                            +memberNumber+"";
            JsonParser jParser = new JsonParser();
            Log.d("-------chetan---------","----------------------");
            String response = jParser.getJSONFromUrl(url1);

            if(response.equals("404")||
                    response.equals("503"))
            {
                return response;
            }
            else
            {
              //  JSONObject checkNumber = jParser.getJSONFromUrl(url1);

                // try parse the string to a JSON object
                try {
                      checkNumber = new JSONObject(response);
                } catch (JSONException e) {
                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                }
            }


            // return JSON String
            Log.d("json...........",checkNumber.toString());

//            memberNumber = params[0];

            try {
                        org = checkNumber.getJSONArray("groups");


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block

                        e.printStackTrace();
                   }


            return org.toString();
            //return isPublisher;
        }





        @Override
        protected void onPostExecute(String response) {

            pd.dismiss();


            if(response.equals("exception"))
            {
                Log.d("server downnnnnnnnnnnn", response);


                myDialogFragment ob2=new myDialogFragment();
                //  Dialog ob1=new Dialog(c);


                android.app.FragmentManager fm=getFragmentManager();
                ob2.show(fm, "1");
// Toast.makeText(DashboardActivity.this,"Unable to connect with server",Toast.LENGTH_SHORT).show();



            }
            else
            {
                Log.d("server okkkkkkkkkkk",response);

                // TODO Auto-generated method stub
                try
                {  org = new JSONArray(response);
                    for(int i=0;i<org.length();i++)
                    {
                        JSONObject orgObject= org.getJSONObject(i);
                        Log.d("org name",orgObject.keys().toString());
                        groupDetails(orgObject);
                    }

                    for(int j=0;j<org_list.size();j++)
                    {
                        Log.d("orgname",org_list.get(j));

                        for(int z=0;z<subgrup_list.get(j).size();z++)
                        {
                            Log.d("subgroup name",subgrup_list.get(j).get(z));

                        }
                    }


                    //     group_spinner= (Spinner) findViewById(com.example.root.cart.R.id.group_spinner);
                    //   subgroup_spinner=(Spinner) findViewById(com.example.root.cart.R.id.subgroup_spinner);
                    // ArrayAdapter<String> orgadapter = new ArrayAdapter<String>(DashboardActivity.this, android.R.layout.simple_spinner_item, orgList);

                    // orgadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    // orgadapter.notifyDataSetChanged();




                }
                catch(JSONException e)
                {

                }

               /* Message msg = Message.obtain();
                msg.obj = isPublisher.toString();
                handler.sendMessage(msg);
*/
                orgList = new String[org_list.size()];

                for (int c = 0; c < org_list.size(); c++) {
                    orgList[c] = org_list.get(c);
                    Log.d("dashboarddddchetanlist", orgList[c]);
                }

                group_spinner.setAdapter(new ArrayAdapter<String>(DashboardActivity.this,
                        R.layout.spinner_row,
                        orgList));


            }





        }
    }

    public final class myDialogFragment extends DialogFragment {

        public Dialog onCreateDialog(Bundle savedInstanceState) {


            Log.d("inside", "test");
            System.out.println("inside on create dialog function");
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            System.out.println("builder value is " + builder);
            builder.setMessage("Unable to connect with server. \n Try again later")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //implementation of OK part

                            finish();

                        }
                    }
                    );
            // Create the AlertDialog object and return it
            return builder.create();
        }

    }

   /* public final class myDialogFragment2 extends DialogFragment {

        public Dialog onCreateDialog(Bundle savedInstanceState) {


            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            System.out.println("builder value is " + builder);
            builder.setMessage("Are you sure you want to delete your Account?")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //implementation of OK part
                                    if (connect()) {
                                        deleteAccount = new DeleteAccount(DashboardActivity.this);
                                        deleteAccount.execute(deleteAccount.toString());
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();

                                    }

                                }
                            }
                    ).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dismiss();
                }
            });

            // Create the AlertDialog object and return it
            return builder.create();
        }

    }*/
   /* public void newOrder(View v){
        Intent i = new Intent(this,NewOrderActivity.class);
        startActivity(i);
    }

    public void previousOrder(View v){
        Intent i = new Intent(this,PreviousOrdersActivity.class);
        startActivity(i);
    }

    public void draft(View v){
        Intent i = new Intent(this,draftActivity.class);
        startActivity(i);
    }

    public void settings(View v){
        Intent i = new Intent(this,SettingsActivity.class);
        startActivity(i);
    }*/


    public void groupDetails(JSONObject object)throws JSONException
    {
      //  ArrayList<String> org_list = new ArrayList<>();
      //  Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            org_name = keysItr.next();
            org_list.add(org_name);
            Object obj = object.get(org_name);
            // if(value instanceof JSONArray) {
            //  value = toList((JSONArray) value);
            JSONArray org_groups = (JSONArray)obj;

         //   List<Object> list = new ArrayList<Object>();
            for(int i = 0; i <org_groups.length(); i++) {
                Object value = org_groups.get(i);
                JSONObject groups = (JSONObject)value;

             //   Map<String, Object> gmap = new HashMap<String, Object>();

                Iterator<String> keys1 = groups.keys();

                while(keys1.hasNext()) {
                    groupid = keys1.next();

                   // Log.d("groupid",groupid);
                    Object groupname = groups.get(groupid);

                    grpname = (String)groupname;

                    group_list.add(grpname);

                    SharedPreferences.Editor editor = sharedPref.edit();

                    editor.putString(org_name+grpname, groupid);
                    editor.putString(groupid,grpname);
                    editor.commit();
                 //   Log.d("groupname",groupname.toString());
                }

                subgrup_list.add(group_list);

               // Log.d("details",org_name+""+groupid+""+grpname);

            }
            //return list;
            // }
        }



        // map.put(key, value);
    }


    //------------------------------------------------------------------------------------------


 	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
        String subgroupList[]= new String[subgrup_list.get(position).size()];

        for(int c=0;c<subgrup_list.get(position).size();c++)
        {
            subgroupList[c]=subgrup_list.get(position).get(c);
        }
        group_spinner.setSelection(position);
       // adapter_state = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, subgroupList);
        adapter_state = new ArrayAdapter<String>(this,R.layout.spinner_row, subgroupList);
        subgroup_spinner.setAdapter(adapter_state);
		Log.d("testing", "inside selection");

        final String orgname = parent.getSelectedItem().toString();

        subgroup_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String grpname =  parent.getSelectedItem().toString();
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("groupId", sharedPref.getString(orgname+grpname,""));
               // editor.putString(sharedPref.getString(orgname+grpname,""),grpname);
                editor.commit();
                //Toast.makeText(DashboardActivity.this, orgname+"   "+grpname+" "+sharedPref.getString(orgname+grpname,""), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean connect(){
		String connection= "false";
	//	ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
		 ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	   	   NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	   	   NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	   	if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
	   		
	   		connection="true";
	   		//saif return connection;
	   	}
	   	//saif else {
            Boolean con = Boolean.valueOf(connection);
            return con;
	   	//saif}

	}
    @Override
    protected void onDestroy() {

        super.onDestroy();
    }
/*
    public class  DeleteAccount extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        Context context;

        public DeleteAccount(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage("Deleting...");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {


            String url = null;
            try {
                url = "http://ruralict.cse.iitb.ac.in/AFC/IvrsServlet?req_type=deleteUser&number="+ memberNumber+"";
            } catch (Exception e) {
                e.printStackTrace();
            }

            JsonParser jParser = new JsonParser();
            Log.d("-------saifur---------", "----------------------");
            response3 = jParser.getJSONFromUrl(url);
            System.out.println("serverrrrrrrrrrrrrrrrrrrrr responsssseeeeeeeeeeeeeeee" + "  " + response3);
            return response3;
            //return isPublisher;*//*
        }





        @Override
        protected void onPostExecute(String response3) {

            pd.dismiss();


            if(response3.equals("exception"))
            {
                Log.d("server downnnnnnnnnnnn", response3);


                myDialogFragment ob2=new myDialogFragment();
                //  Dialog ob1=new Dialog(c);


                android.app.FragmentManager fm=getFragmentManager();
                ob2.show(fm, "1");
// Toast.makeText(DashboardActivity.this,"Unable to connect with server",Toast.LENGTH_SHORT).show();



            }
            else
            {
                try {
                    JSONObject obj = new JSONObject(response3);
                    String msg = obj.getString("status");


                    System.out.println("deeeeleeeteee usseeerrrr" + response3);

                    if(msg.equals("success")) {
                        Toast.makeText(getApplicationContext(), "Account Deleted", Toast.LENGTH_SHORT).show();
                        finish();

                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Sorry, please try again later", Toast.LENGTH_SHORT).show();

                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }

        }
    }*/


}
