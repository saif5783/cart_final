package com.example.root.cart;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends Activity {

    Intent i;
    String memberNumber="",otp_check;
    EditText  number,otp;
    TextView tv;
    MobileNumberVerification mv;
    Button otp_btn,confirm_btn,cancel_btn;
    int dialogid;
    long session;
    OTPCountDownTimer countDownTimer;
    String response;
    private RetainedFragment dataFragment;
    boolean login;
    SharedPreferences sharedPref;

    // AlertDialog signUpDlg;
     Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        login = sharedPref.getBoolean("login", false);
        if (login) {
            Intent i = new Intent(MainActivity.this, DashboardActivity.class);
            startActivity(i);
            finish();
        } else {

            setContentView(R.layout.activity_main);

            tv = (TextView) findViewById(R.id.head);
            String fontPath = "fonts/trench100free.otf";
            Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
            tv.setTypeface(tf);
//        tv.setTextSize(35);

            FragmentManager fm = getFragmentManager();
            dataFragment = (RetainedFragment) fm.findFragmentByTag("MainActivity");


            if (dataFragment == null) {
                System.out.println("firstttttttttt timeeeeeeeeeeeeeeeeeeeeee");
                dataFragment = new RetainedFragment();
                fm.beginTransaction().add(dataFragment, "MainActivity").commit();

                dialogid = -1;
                dataFragment.setdialogId(dialogid);
                // dialog = new Dialog(MainActivity.this);

            } else {
                System.out.println("seconddddddddddddddd timeeeeeeeeeeeeeeeeeeeeee");

                if (dataFragment.getdialogId() == 1) {
                    System.out.println(dataFragment.getdialogId());

                    ((Button) findViewById(R.id.signUpBtn)).performClick();
                }

            }


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case com.example.root.cart.R.id.action_home:
                return true;
            case com.example.root.cart.R.id.action_settings:
                Intent intent = new Intent(this,OverflowActivity.class);
                startActivity(intent);
                return true;

            /*case R.id.action_delete:{
                myDialogFragment2 ob=new myDialogFragment2();
                //  Dialog ob1=new Dialog(c);


                android.app.FragmentManager fm=getFragmentManager();
                ob.show(fm, "1");
            }*/

            default:
                return super.onOptionsItemSelected(item);
        }

        //return super.onOptionsItemSelected(item);
    }



    public void signIn(View v)
    {
        if(connect())
        {
            i =  new Intent(MainActivity.this,LoginActivity.class);
            startActivityForResult(i, 0);
          //  i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //startActivity(i);
            //finish();
        }
        else
        {
            Toast.makeText(this,"Please check WIFI/Mobile data connection",Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(resultCode)
        {
            case 0:
                setResult(0);
                finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("startedddddddddddddddddddddddddd");
        // editor.clear();
        //editor.commit();
    }


    @Override
    protected void onResume() {
        super.onResume();

        System.out.println("resssssssssssssummmmmmmmmmmmmmeeeeeeeeeee");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("pausedddddddddddddddddddddddddd");


        dataFragment.setdialogId(dialogid);



    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("restarrrrrrrrrrrrrrrrrrtttttttttttttttttttttttt");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("stoooooooooooopppppppppppppppeddddd");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.out.println("killlllllllllllllllllllllllinnnnnnnnnnnnnnnnnnnnngggggggggggggggggggggggggggg");

        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();

       /* dataFragment.setPos(grpposition);
       // if(organizationDialog.isShowing())

            dataFragment.setdialogId(dialogid);

      if(dialogid==2)
      {
          for (Subgroup p : sublistAdapter.getBox()) {


              editor.putBoolean("subgrp_pos_status_ondestroy" + grpposition + p.position,p.box);
              editor.commit();
          }

          dataFragment.setGroupDialogFlag(true);
      }*/


        //   if(groupDialog.isShowing())

        //  dataFragment.setdialogId(dialogid);


        //  editor.clear();
        //editor.commit();
    }


    public void signUp(View v)
    {
        dialogid=1;

        if(connect())
        {

         //  AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
            dialog = new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.authentication_box);
            dialog.setTitle("Sign Up");
            dialog.setCanceledOnTouchOutside(false);
            // set the custom dialog components - text, image and button
            number = (EditText) dialog.findViewById(R.id.number);

            otp = (EditText) dialog.findViewById(R.id.OTP);
            otp.setVisibility(View.GONE);

            otp_btn = (Button)dialog. findViewById(R.id.buttonOTP);

            confirm_btn = (Button)dialog.findViewById(R.id.buttonSubmit);
            confirm_btn.setVisibility(View.GONE);

            cancel_btn = (Button)dialog. findViewById(R.id.buttoncancel);

            otp_btn.setOnClickListener(new View.OnClickListener()


                                          {
                                              @Override
                                              public void onClick(View v) {

                                                  if(!number.getText().toString().equals(""))
                                                  {

                                                      long i = Long.parseLong(number.getText().toString());
                                                      long length = (long)(Math.log10(i)+1);
                                                      if(length<10 || length > 10){
                                                          Toast.makeText(getApplicationContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
                                                      }
                                                      else{

                                                          memberNumber=number.getText().toString();
                                                          mv = new MobileNumberVerification(MainActivity.this);
                                                          mv.execute(number.getText().toString());

                                                      }
                                                  }
                                                  else
                                                  {
                                                      Toast.makeText(getApplicationContext(), "Please enter mobile number", Toast.LENGTH_SHORT).show();
                                                      otp.setEnabled(false);

                                                  }

                                              }

                                              // Perform button logic
                                          }
                );

            confirm_btn.setOnClickListener(new View.OnClickListener()


                                          {
                                              @Override
                                              public void onClick(View v) {

                                                  long session2 = System.currentTimeMillis();
                                                  if(session2<session)
                                                  {
                                                     if(otp.getText().toString().equals(otp_check))
                                                     {
                                                         countDownTimer.cancel();
                                                         i =  new Intent(MainActivity.this,SignUpActivity.class);
                                                         i.putExtra("json",response);
                                                         i.putExtra("mobilenumber",memberNumber);
                                                         startActivity(i);
                                                         finish();
                                                     }
                                                      else
                                                     {
                                                         otp.setHint("Enter the OTP recieved");
                                                        // otp.setText(null);
                                                         Toast.makeText(getApplicationContext(), "Please enter correct OTP", Toast.LENGTH_SHORT).show();

                                                     }
                                                  }
                                                  else
                                                  {
                                                      otp.setText("");
                                                      otp.setHint("Enter the OTP recieved");
                                                      otp.setEnabled(false);
                                                      otp_btn.setEnabled(true);
                                                      cancel_btn.setEnabled(true);
                                                      Toast.makeText(getApplicationContext(), "Sorry,your OTP expires.Try to get new OTP", Toast.LENGTH_SHORT).show();

                                                  }


                                              }

                                              // Perform button logic
                                          }



            );
            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();

                }
            });

            dialog.show();


           /* i =  new Intent(MainActivity.this,SignUpActivity.class);
            startActivity(i);*/
            //  finish();
        }
        else
        {
            Toast.makeText(this,"Please check WIFI/Mobile data connection",Toast.LENGTH_SHORT).show();
        }
    }


    public boolean connect(){
        String connection= "false";
        //	ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){

            connection="true";
            //saif return connection;
        }
        //saif else {
        Boolean con = Boolean.valueOf(connection);
        return con;
        //saif}

    }

    //----------------------------------------------------------------------------------------


    public class MobileNumberVerification extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        Context context;

        public MobileNumberVerification(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
          //  this.handler = handler;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            lockScreenOrientation();
            pd = new ProgressDialog(context);
            pd.setMessage("Checking number......");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {



            String url =
                    "http://ruralict.cse.iitb.ac.in/AFC/IvrsServlet?req_type=newUser&number="
                            +params[0]+"";

            JsonParser jParser = new JsonParser();
            Log.d("-------chetan---------","----------------------");
             response = jParser.getJSONFromUrl(url);


            return response;
            //return isPublisher;*/
        }





        @Override
        protected void onPostExecute(String response) {

            pd.dismiss();


            if(response.equals("exception"))
            {
                Log.d("server downnnnnnnnnnnn", response);


                myDialogFragment ob2=new myDialogFragment();
                //  Dialog ob1=new Dialog(c);


                android.app.FragmentManager fm=getFragmentManager();
                ob2.show(fm, "1");
// Toast.makeText(DashboardActivity.this,"Unable to connect with server",Toast.LENGTH_SHORT).show();



            }
            else
            {

                System.out.println("mainnnnnnnnnnnactivittttyyyyyyyyyyyyyyyyyy"+response);


                try {
                    JSONObject obj = new JSONObject(response);

                    String msg = obj.getString("msg");

                    if(msg.equals("success"))
                    {
//                        otp.setEnabled(true);
                        otp.setVisibility(View.VISIBLE);
                        confirm_btn.setVisibility(View.VISIBLE);
                        number.setEnabled(false);
                        otp_check = obj.getString("otp");
//                        cancel_btn.setEnabled(false);
                        otp_btn.setVisibility(View.GONE);
                        cancel_btn.setVisibility(View.GONE);

//                        otp_btn.setEnabled(false);
                        session = System.currentTimeMillis();
                        countDownTimer = new OTPCountDownTimer(600000, 1000);
                        countDownTimer.start();
                        session = session+600000;

                    }

                    else
                    {

                        otp.setEnabled(false);

                        number.setText(null);
                        Toast.makeText(getApplicationContext(), "You are already registered user", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


               /* Log.d("server okkkkkkkkkkk",response);

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("orgGroup",response);
                editor.commit();

                Message msg = Message.obtain();
                msg.obj = isPublisher.toString();
                handler.sendMessage(msg);*/

            }
            unlockScreenOrientation();




        }
    }

    private void lockScreenOrientation() {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void unlockScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    public final class myDialogFragment extends DialogFragment {

        public Dialog onCreateDialog(Bundle savedInstanceState) {


            Log.d("inside", "test");
            System.out.println("inside on create dialog function");
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            System.out.println("builder value is " + builder);
            builder.setMessage("Unable to connect with server. \n Try again later")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //implementation of OK part

                                    finish();

                                }
                            }
                    );
            // Create the AlertDialog object and return it
            return builder.create();
        }


    }


    // CountDownTimer class
    public class OTPCountDownTimer extends CountDownTimer
    {

        public OTPCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
            otp.setText("");
            otp.setHint("Enter the OTP recieved");
            otp.setEnabled(false);
            cancel_btn.setEnabled(true);
            otp_btn.setEnabled(true);
            Toast.makeText(getApplicationContext(), "Sorry,your OTP expires.Try to get new OTP", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onTick(long millisUntilFinished)
        {
        }
    }

}
